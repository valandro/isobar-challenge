[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

# API ISOBAR
Application to consumes data from Isobar API.

### Running

Building the project:

```
./gradlew clean build
```

Running the project:

```
./gradlew bootrun
```

### API

Request:

```
http://localhost:9000/artist
```

You could pass some parameters to filter and order the response, they are all **optional**


```
http://localhost:9000/artist?order=true&name=Ad&popularity=false
```


### Enhancement

Possible enhancements for the project:

- Before make the request to the **Isobar API**, the application could make a search on a **NoSQL database**
that will be used as a cache system. If the information was that, the application will not make the request 
and will be more fastier.

### Environment
 - Java JDK 10
 - Gradle 4.3 or higher 

 
### License
MIT License. [Click here for more information.](LICENSE)
