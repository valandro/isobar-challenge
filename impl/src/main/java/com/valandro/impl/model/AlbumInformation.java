package com.valandro.impl.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AlbumInformation {
    private String name;
    private String image;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime releasedDate;
    private String band;
    private List<TracksInformation> tracks;
    private String id;
}
