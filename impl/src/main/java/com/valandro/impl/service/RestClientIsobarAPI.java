package com.valandro.impl.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valandro.impl.exception.ImplException;
import com.valandro.impl.model.Artist;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class RestClientIsobarAPI {
    @Value("${isobar.api.url}")
    private String url;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    public List<Artist> findBandsInformation() {
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        try {
            return Optional.ofNullable(objectMapper.readValue(response.getBody(), Artist[].class))
                           .map(Arrays::asList)
                           .orElse(new ArrayList<>());
        } catch (IOException ex) {
            log.error("Error parsing JSON from Isobar API.");
            throw new ImplException(HttpStatus.INTERNAL_SERVER_ERROR, "Error parsing json");
        } catch (HttpStatusCodeException ex) {
            throw new ImplException(HttpStatus.BAD_GATEWAY, ex.getMessage());
        }
    }
}
