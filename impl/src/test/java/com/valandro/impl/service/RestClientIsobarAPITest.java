package com.valandro.impl.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.valandro.impl.model.Artist;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class RestClientIsobarAPITest {
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private RestClientIsobarAPI restClientIsobarAPI;

    @Before
    public void setUp() {
        ReflectionTestUtils.setField(restClientIsobarAPI, "url", "https://iws-recruiting-bands.herokuapp.com/api/full");
        objectMapper = new ObjectMapper();
    }

    @Test
    public void shouldReturnEmptyArrayWhenApiResponseIsEmpty() {
        HttpHeaders header = new HttpHeaders();
        ResponseEntity<String> apiResponse = new ResponseEntity<>("{}", header, HttpStatus.OK);
        String url = "https://iws-recruiting-bands.herokuapp.com/api/full";
        Mockito.doReturn(apiResponse)
                .when(restTemplate)
                .getForEntity(url, String.class);

        List<Artist> result = restClientIsobarAPI.findBandsInformation();
        assertNotNull(result);
        assertTrue(result.isEmpty());
    }
}