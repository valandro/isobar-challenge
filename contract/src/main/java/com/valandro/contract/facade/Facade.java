package com.valandro.contract.facade;

import com.valandro.contract.exception.ApiException;
import com.valandro.impl.model.Artist;
import com.valandro.impl.service.RestClientIsobarAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class Facade {
    @Autowired
    private RestClientIsobarAPI restClientIsobarAPI;

    public List<Artist> getArtistInformation(String artistName, Boolean order, Boolean popularity) {
        List<Artist> artists = restClientIsobarAPI.findBandsInformation();
        return this.applyFilters(artists, artistName, order, popularity);
    }

    private List<Artist> applyFilters(List<Artist> artists, String artistName, Boolean order, Boolean popularity) {
        return Optional.ofNullable(artists)
                       .filter(list -> !list.isEmpty())
                       .map(artistList ->
                           artistList.stream()
                                      .filter(art -> filterArtistNameLike(art, artistName))
                                      .collect(Collectors.toList()))
                       .filter(artistsList -> !artistsList.isEmpty())
                       .map(artistList -> sortByPopularity(artistList, popularity))
                       .map(artistList -> orderByName(artistList, order))
                       .orElseThrow(() -> new ApiException(HttpStatus.NOT_FOUND, "No artist was found."));
    }

    private List<Artist> orderByName(List<Artist> artistList, Boolean order) {
        return Optional.of(order)
                       .filter(Boolean::booleanValue)
                       .map(o ->
                               artistList.stream()
                                         .sorted(Comparator.comparing(Artist::getName))
                                         .collect(Collectors.toList()))
                       .orElse(artistList);
    }

    private List<Artist> sortByPopularity(List<Artist> artistList, Boolean popularity) {
        return Optional.of(popularity)
                .filter(Boolean::booleanValue)
                .map(p ->
                        artistList.stream()
                                .sorted(Comparator.comparingLong(Artist::getNumPlays).reversed())
                                .collect(Collectors.toList()))
                .orElse(artistList);
    }

    private Boolean filterArtistNameLike(Artist artist, String artistName) {
        if (Objects.isNull(artistName)) return true;
        return Optional.of(artistName)
                       .filter(name -> name.contains(artist.getName()))
                       .isPresent();
    }

}
