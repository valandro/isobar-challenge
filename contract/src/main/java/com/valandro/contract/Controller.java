package com.valandro.contract;

import com.valandro.contract.facade.Facade;
import com.valandro.impl.model.Artist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class Controller {
    @Autowired
    private Facade facade;

    @GetMapping(value = "/artists",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Artist> getBandsInformation(
            @RequestParam(value = "name", required = false) String artistName,
            @RequestParam(value = "order", defaultValue = "false", required = false) Boolean order,
            @RequestParam(value = "popularity", defaultValue = "false", required = false)  Boolean popularity) {
        return facade.getArtistInformation(artistName, order, popularity);
    }
}
