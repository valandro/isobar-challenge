package com.valandro.contract.facade;

import com.valandro.contract.exception.ApiException;
import com.valandro.contract.stub.ArtistStub;
import com.valandro.impl.model.Artist;
import com.valandro.impl.service.RestClientIsobarAPI;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FacadeTest {
    @InjectMocks
    private Facade facade;

    @Mock
    private RestClientIsobarAPI restClientIsobarAPI;

    @Test(expected = ApiException.class)
    public void noArtistWasFound() {
        List<Artist> artists = new ArrayList<>();
        String artistName = "Paramore";
        Boolean order = false;
        Boolean popularity = false;

        doReturn(artists)
                .when(restClientIsobarAPI)
                .findBandsInformation();

        facade.getArtistInformation(artistName, order, popularity);
    }

    @Test(expected = ApiException.class)
    public void noArtistWithNameLikeWasFound() {
        List<Artist> artists = ArtistStub.getArtistList();
        String artistName = "Artic Monkey";
        Boolean order = false;
        Boolean popularity = false;

        doReturn(artists)
                .when(restClientIsobarAPI)
                .findBandsInformation();

        facade.getArtistInformation(artistName, order, popularity);
    }

    @Test
    public void oneArtistWillBeFoundWithNameLike() {
        List<Artist> artists = ArtistStub.getArtistList();
        String artistName = "Paramore Band";
        Boolean order = false;
        Boolean popularity = false;

        doReturn(artists)
                .when(restClientIsobarAPI)
                .findBandsInformation();

        List<Artist> result = facade.getArtistInformation(artistName, order, popularity);
        assertNotNull(result);
        assertTrue(!result.isEmpty());
    }
}