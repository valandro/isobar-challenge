package com.valandro.contract.stub;

import com.valandro.impl.model.AlbumInformation;
import com.valandro.impl.model.Artist;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ArtistStub {
    public static List<Artist> getArtistList() {
        return Collections.singletonList(getArtist());
    }

    public static Artist getArtist() {
        return Artist.builder()
                .name("Paramore")
                .genre("Rock")
                .id("58c15e07a7ec6204001f0470")
                .numPlays(12319230L)
                .albumList(Collections.singletonList(Collections.singletonList(getAlbumInformation())))
                .albuns(Collections.singletonList("58c15e07a7ec6204001f0448"))
                .build();
    }

    public static AlbumInformation getAlbumInformation() {
        return AlbumInformation.builder()
                .id("58c15e07a7ec6204001f0448")
                .name("Brand new eye")
                .releasedDate(LocalDateTime.of(2016, 12, 12, 12, 30))
                .build();
    }
}
